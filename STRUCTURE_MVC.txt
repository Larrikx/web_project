
MVC: La structure des dossiers
- app
	- routeur.php
		1. Lance une action d'un CTRL
	- config
		- params.php
			1. Initialise les zones dynamiques
			2. Défini les paramètres de connexion à la db
	- controleurs
		- postsControleur.php
			1. Demande des infos au modèle
			2. Charge la vue dans la zone dynamique

	-vues
		-posts
			-...
		-template
			-index.php
			-partials
- noyau
	- init.php
		1. Charger le fichier ./app/config/params.php
		2. Charger le fichier ./noyau/connexion.php
	- connexion.php
		1. Fait la connexion à la db
- www
	- index.php DISPATCHER CENTRAL
		1. Charger le fichier  ./noyau/init.php
		2. Charger le routeur  ./app/routeur.php
		3. Charger le template ./app/vues/template/index.php
	- .htaccess RE-ECRITURE D'URL
